"use client";
import React from "react";
import { UserAuth } from "./contexts/AuthContext";
import Spinner from "./components/Spinner";
import { HomeOutlined } from "@ant-design/icons";
import { Divider } from "antd";
import Carousel from "./components/BannerSlider";
import Banner from "./components/Banner";

export default function Home() {
  const { loading } = UserAuth();
  const opts = {
    height: '390',
    width: '640',
    playerVars: {
      autoplay: 0,
    },
  };

  return (
    <main>
      {loading ? (
        <Spinner />
      ) : (
        <>
          <div
        data-aos="fade-zoom-in"
        data-aos-offset="200"
        data-aos-easing="ease-in-sine"
        data-aos-duration="600"
      >
        <Carousel></Carousel>
      </div>
      <Banner></Banner>

          <Divider />
        </>
      )}
    </main>
  );
}
