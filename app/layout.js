"use client";
import "./globals.css";
import { Inter } from "next/font/google";
import { AuthContextProvider } from "./contexts/AuthContext";
import Navbar from "./components/Navbar";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <AuthContextProvider>
        <body className="flex w-full ">
          <div className="min-h-[500px] w-full ">
            <Navbar></Navbar>
            <main className=" bg-white rounded-lg">
              {children}
            </main>
          </div>
        </body>
      </AuthContextProvider>
    </html>
  );
}
